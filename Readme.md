Docker example (require docker, docker-compose):  
    * Go to example directory `cd example`  
    * Build image `docker build -t pp/cv .`  
    * Install composer `docker run -it -v $(pwd):/data pp/cv composer install`  
    * Run example:  
        > `docker-compose up --build -d`  
        > `curl http://localhost:1201`

Non-docker example (require composer, curl, php 7.1+):  
    * Go to example directory `cd example`  
    * Install composer `composer install`  
    * Run example:  
        > `php -S localhost:1201`  
        > `curl http://localhost:1201`  