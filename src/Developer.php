<?php

namespace Gaffa;

use Gaffa\Utils\Printer;

class Developer implements DeveloperInterface
{
    /** @var array  */
    private $personalInfo = [];
    /** @var array  */
    private $education = [];
    /** @var array  */
    private $experience = [];
    /** @var array  */
    private $skills = [];

    /**
     * Developer constructor.
     */
    public function __construct()
    {
        $this->initPersonalInfo();
        $this->initEducations();
        $this->initExperience();
        $this->initSkills();
    }

    /** Fill personal information */
    public function initPersonalInfo()
    {
        $this->personalInfo = [
            'Name' => 'Paweł Podlasiński',
            'Birth' => '03.10.1985',
            'Email' => 'pawel@podlasinski.pl',
            'Phone' => '667617977'
        ];
    }

    /** Fill experience array */
    public function initExperience()
    {
        $this->experience = [
            '>2000' => 'Small websites in PHP and JavaScript',
            '2006' => 'Database developer in "ALBIT technical service"',
            '2006 - 2007' => sprintf('%s %s', 'Representing Poland during the world finals in Microsoft Imagine Cup.',
                'Artificial Intelligence in RTS game. Finished in top 4.'),
            '2008 - 2009' => sprintf('%s %s', 'Representing Poland in Microsoft Imagine Cup.',
                'Artificial Intelligence in Robotics. Finished in top 8.'),
            '2010 - 2014' => sprintf('%s %s', 'Architect and developer in Revelco Sp. z o.o.',
                'Responsible for creating and run Walutomat.pl - first Polish social currency exchange market.'),
            '2014 - 2016' => 'Php developer in Bitnoise SC. Mainly responsible for creating CRM systems for clients.',
            '2016 - today' => 'Senior PHP developer and team/tech leader of payments department in Kinguin Sp. z o.o.'
        ];
    }

    /** Fill education array */
    public function initEducations()
    {
        $this->education = [
            '2000 - 2004' => 'IV High school in Kielce',
            '2004 - 2008' => sprintf('%s %s', 'Computer science studies at Kielce University of Technology.',
                'Engineers degree with work of "Massive Multiplayer Online Strategy Game"'),
            '2008 - 2010' => sprintf('%s %s', 'Computer science studies at Poznań University of Technology.',
                'Master degree with work of "Artificial Intelligence implementation in robotics"')
        ];
    }

    /** Fill skills array */
    public function initSkills()
    {
        $this->skills = [
            'Object oriented programming, mvc and project patterns',
            'Relational databases like MySQL and no-sql databases like mongodb',
            'Controlling version systems: Git, SVN',
            'Experience with PHP frameworks (mainly Symfony)',
            'Experience with JavaScript frameworks and libraries',
            'Practical knowledge about data exchanging: JSON, XML, ajax, RESTful API',
            'Data caching systems: memcache, redis',
            'Knowledge about dev-ops stuff: Docker',
            'Basic knowledge of programming language: C/C++, Java, C#',
            'English skills allowing to read and write technical documentation'
        ];
    }

    /** Print data */
    public function print(): void
    {
        $printer = new Printer();
        $printer->clear();
        $printer->print('Personal Information:', $this->personalInfo);
        $printer->print('Education', $this->education);
        $printer->print('Experience', $this->experience);
        $printer->print('Skills', $this->skills);
        $printer->clear();
    }
}