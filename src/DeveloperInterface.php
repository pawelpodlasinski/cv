<?php

namespace Gaffa;

interface DeveloperInterface
{
    /** Fill experience array */
    public function initPersonalInfo();
    /** Fill experience array */
    public function initExperience();
    /** Fill education array */
    public function initEducations();
    /** Fill skills array */
    public function initSkills();
}