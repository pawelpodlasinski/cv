<?php

namespace Gaffa\Utils;

class Printer
{
    /**
     * @param null|string $header
     * @param array|null $data
     */
    public function print(?string $header, ?array $data): void
    {
        $leadingColumn = empty($data[0]);
        $mask = $leadingColumn ? "%15.50s | %-30s%s" : " * %15s%s";
        echo sprintf("%s:%s%s", $header, PHP_EOL, PHP_EOL);
        foreach ($data as $key => $item) {
            echo $leadingColumn ? sprintf($mask, $key, $item, PHP_EOL) : sprintf($mask, $item, PHP_EOL);
        }
        $this->clear();
    }

    /**
     * @param null|string $header
     * @param array|null $data
     */
    public function printSkills(?string $header, ?array $data): void
    {
        $mask = " * %15s%s";
        echo sprintf("%s:%s%s", $header, PHP_EOL, PHP_EOL);
        foreach ($data as $key => $item) {
            echo sprintf($mask, $item, PHP_EOL);
        }
        $this->clear();
    }

    /**
     * Add two end of lines characters
     */
    public function clear(): void
    {
        echo PHP_EOL . PHP_EOL;
    }
}